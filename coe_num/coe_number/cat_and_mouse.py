def cat_and_mouse(x, y, z):
    ans = "Mouse C"
    a = abs(x - z)
    b = abs(y - z)
    
    if a < b:
        ans = "Cat A"
    elif b < a:
        ans = "Cat B"
    return ans


if __name__=="__main__":
    m = int(input())
    for m_itr in range(m):
        xyz = input().split()
        x = int(xyz[0])
        y = int(xyz[1])
        z = int(xyz[2])
        ans = cat_and_mouse(x, y, z)
        print(ans)
