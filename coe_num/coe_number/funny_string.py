def funny_string(a):
    x = a[::-1]
    b = []
    for i in range(len(a) - 1):
        b.append(abs(ord(x[i]) - ord(x[i+1])))
    if b[::] == b[::-1]:
        return "Funny"
    else:
        return "Not Funny"

if __name__ == '__main__':
    c = int(input().strip())

    for c_m in range(c):
        c = input()
        ans = funny_string(c)
    
