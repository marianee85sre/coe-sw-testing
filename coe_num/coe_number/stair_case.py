     
def stair_case(numb, pattern):

    a = ""
    if 0 < numb <= 30 and numb == numb // 1:
        for i in range(1, numb + 1):
            a += (" " * (numb - i)) + (f"{pattern}" * i) + "\n"
        return a[:-1]

    return False

