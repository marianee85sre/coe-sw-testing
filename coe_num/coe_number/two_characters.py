def two_char_test(m, n):
    max = 0
    count = 0

    set = list(set(n))

    if 1 <= m <= 1000:
        for i in range(len(set)):
            for j in range(i + 1, len(set)):
                index_list = [set[i], set[j]]
                if n.index(set[i]) < n.index(set[j]):
                    index_set = 0
                else:
                    index_set = 1

                for k in n:
                    if k in index_list:
                        if k == index_list[index_set]:
                            count += 1
                            index_set = index_set ^ 1
                        else:
                            count = 0
                            break

                max = max(max, count)
                count = 0

        return max
    else:
        return "Out of value sir!"
