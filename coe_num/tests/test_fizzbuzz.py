from coe_number.fizzbuzz import Fizzbuzz as fb

import unittest

class FizzbuzzTest(unittest.TestCase):
    def test_give_15_30_(self):
        num_list = [15,30]
        FizzBuzz = fb(num_list)
        self.assertTrue(FizzBuzz)

    def test_give_3_6_9_(self):
        num_list = [3,6,9]
        Fizz = fb(num_list)
        self.assertTrue(Fizz)

    def test_give_5_10_15_(self):
        num_list = [5,10,15]
        Buzz = fb(num_list)
        self.assertTrue(Buzz)
