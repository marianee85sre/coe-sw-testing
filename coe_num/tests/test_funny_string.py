from coe_number.funny_string import funny_string
import unittest

class FunntString(unittest.TestCase):
    def test_give_ABC_is_Funny_string(self):
        word = 'ABC'
        ans = "Funny"
        result = funny_string(word)
        self.assertEqual(result, ans)

    def test_give_MARIA_is_Funny_string(self):
        word = 'MARIA'
        ans = "Not Funny"
        result = funny_string(word)
        self.assertEqual(result, ans)

    def test_give_XYZ_is_Funny_string(self):
        word = 'XYZ'
        ans = "Funny"
        result = funny_string(word)
        self.assertEqual(result, ans)

    def test_give_TUM_is_Funny_string(self):
        word = 'TUM'
        ans = "Not Funny"
        result = funny_string(word)
        self.assertEqual(result, ans)

    def test_give_FEN_is_Funny_string(self):
        word = 'FEN'
        ans = "Not Funny"
        result = funny_string(word)
        self.assertEqual(result, ans)

    def test_give_DEF_is_Funny_string(self):
        word = 'DEF'
        ans = "Funny"
        result = funny_string(word)
        self.assertEqual(result, ans)

    def test_give_FUNNY_is_Funny_string(self):
        word = 'FUNNY'
        ans = "Not Funny"
        result = funny_string(word)
        self.assertEqual(result, ans)

    def test_give_MNO_is_Funny_string(self):
        word = 'MNO'
        ans = "Funny"
        result = funny_string(word)
        self.assertEqual(result, ans)
