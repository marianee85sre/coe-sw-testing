from coe_number.number_utils import is_prime_list

import unittest

class PrimeListTest(unittest.TestCase):
    def test_give_1_2_3_is_prime(self):
        prime_list = [1,2,3]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)

    def test_give_4_5_6_is_prime(self):
        prime_list = [4,5,6]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
    
    def test_give_7_8_9_is_prime(self):
        prime_list = [7,8,9]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)

    def test_give_3_5_7_is_prime(self):
        prime_list = [-3,-5,-7]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
    
    def test_give_1_31_is_prime(self):
        prime_list = [1, 2, 3, 4, 5, 6, 7, 8, 9,
        10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
    
    def test_give_2_3_7_11r_is_prime(self):
        prime_list = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)
    
    def test_give_6_4_1_0_1_1_0_7_0_6_is_prime(self):
        prime_list =[6, 4, 1, 0, 1, 1, 0, 7, 0, 6]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
