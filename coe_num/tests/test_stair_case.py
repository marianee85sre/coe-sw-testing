from coe_number.stair_case import stair
import unittest


class StairTest(unittest.TestCase):
    def test_give_4_with_shoudd_be_hh(self):
        numb = 4
        pattern = "#"
        expected_output = " #\n" "   ##\n" + "  ###\n" + " ####"
        
        result = stair(numb, pattern)
        self.assertEqual(result, expected_output, f"Should be {expected_output}")


    def test_give_5_with_shoudd_be_hh(self):
        numb = 5
        pattern = "#"
        expected_output = "    #\n" + "   ##\n" + "  ###\n" + " ####\n" + "#####"
        
        result = stair(numb, pattern)
        self.assertEqual(result, expected_output, f"Should be {expected_output}")
        

    def test_give_22_with_shoudd_be_hh(self):
        numb = 22
        pattern = "#"
        expected_output = False
        
        result = stair(numb, pattern)
        self.assertEqual(result, expected_output, f"Should be {expected_output}")


    def test_give_7_with_shoudd_be_hh(self):
        numb = 7
        pattern = "#"
        expected_output = "       #\n" + "      ##\n" + "     ###\n" + "    ####\n" + "   #####\n" + "  ######\n" + " #######"
        
        result = stair(numb, pattern)
        self.assertEqual(result, expected_output, f"Should be {expected_output}")


    def test_give_6_point_6_with_shoudd_be_hh(self):
        numb = 6.6
        pattern = "#"
        expected_output = False
        
        result = stair(numb, pattern)
        self.assertEqual(result, expected_output, f"Should be {expected_output}")
    
    
    def test_give_native_32_with_shoudd_be_hh(self):
        numb = -32
        pattern = "#"
        expected_output = False
        
        result = stair(numb, pattern)
        self.assertEqual(result, expected_output, f"Should be {expected_output}")

    def test_give_0_point_9_with_shoudd_be_hh(self):
        numb = 0.9
        pattern = "#"
        expected_output = False
        
        result = stair(numb, pattern)
        self.assertEqual(result, expected_output, f"Should be {expected_output}")








